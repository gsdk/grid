<?php

namespace Gsdk\Grid\Renderer;

use Gsdk\Grid\GridBuilder;

class Renderer
{
    protected View\AbstractTable $view;

    public function __construct(array $options = [])
    {
        if (isset($options['renderer'])) {
            $this->view = $this->viewFactory($options['renderer'], $options);
        } elseif (isset($options['view'])) {
            $this->view = $this->viewFactory($options['view'], $options);
        } else {
            $this->view = $this->viewFactory('table', $options);
        }
    }

    public function render(GridBuilder $grid): string
    {
        return $this->view->render($grid);
    }

    private function viewFactory($type, $options)
    {
        $class = __NAMESPACE__ . '\View\\';
        $class .= match (strtolower($type)) {
            'tree' => 'Tree',
            'table' => 'Table',
            default => throw new \Exception('Unavailable renderer type')
        };

        return new $class($options);
    }
}
